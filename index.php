<!DOCTYPE html>
<html>
<head>
    <title>Domaci</title>
    <meta name="author" content="Luka Matkovic">
    <meta name="keywords" content="PHP, variables, functions, string, integer, array, float">
</head>
<body>
    <h2>PHP promenljive</h2>
    <?php

    echo    '<strong>String</strong> - A string is series of characters, where a character is the same as a byte. The simplest way to specify a string is to enclose it in single or double quotes.';
    echo    "<br>";
    echo    'example: <strong><em>$string=<span style="color:red">"Ovo je primer string-a"</span>;</em></strong>';
    echo    "<br>";
    echo    '<strong>Integer</strong> - An integer is a number of the set ℤ = {..., -2, -1, 0, 1, 2, ...}. Integers can be specified in decimal (base 10), hexadecimal (base 16), octal (base 8) or binary (base 2) notation, optionally preceded by a sign (- or +).<br>';
    echo    'example: <strong><em>$int=<span style="color:blue">428</span>;</em></strong>';
    echo    "<br>";
    echo    '<strong>Float</strong> - Floating point numbers (also known as "floats", "doubles", or "real numbers") are numbers with decimals.<br>';
    echo    'example: <strong><em>$float=<span style="color:green">-6.125</span>;</em></strong>';
    echo    "<br>";
    echo    '<strong>Bool</strong> - This is the simplest type. A boolean expresses a truth value. It can be either TRUE or FALSE.';
    echo    "<br>";
    echo    'example: <strong><em>$bool=<span style="color:ORANGE">FALSE</span>;</em></strong>';
    echo    "<br>";
    echo    '<strong>Aray</strong> - An array is a data structure that stores one or more similar type of values in a single value.';
    echo    "<br>";
    echo    'example: <strong><em>$niz=<span style="color:brown">array(2,8,15)</span>;</em></strong>';

 
    ?>
    <h2>Funkcije - primer</h2>

    <?php
    include 'functions.php';
    echo "<p>Zbir brojeva 1, 2, 3, 4, 5 ,6, 7, 8 i 9 iznosi "
            .zadatak("green",array(1,2,3,4,5,6,7,8,9))
            .".</p>";
    echo "<p>Zbir brojeva 2, 5, 9 i 15 iznosi "
            .zadatak("red",array(2,5,9,15))
            .".</p>";
    echo "<p>Zbir brojeva -5, -3, 5, 2 i -3.5 iznosi "
            .zadatak("yellow",array(-5,-3,5,2,-3.5))
            .".</p>";
    echo "<p>Zbir brojeva 5, 121, -19 i -56 iznosi "
            .zadatak("blue",array(5,121,-19,-56))
            .".</p>";
echo "Najmanji broj u nizu (5,121,-19,-56, 9) iznosi" .minimum("red",array(5,121,-19,-56,9));
echo "<br>";
echo "Najveci broj u nizu (5,121,-19,-56, 9) iznosi" .maximum("green",array(5,121,-19,-56, 9));
echo "<br>";
echo "NIZ (2,6,-54, -12.6, 12, 9)";
echo "<br>";
echo minimum("orange", array(2,6,-54, -12.6, 12, 9))." je najmanji broj ovog niza.";
echo "<br>";
echo maximum("grey", array(2,6,-54, -12.6, 12, 9))." je najveci broj ovog niza.";


    ?>
    
</body>
</html>
