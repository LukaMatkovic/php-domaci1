		Domaci zadatak
1. Napraviti web stranicu koja sadr�i definicije promeljnivih tipa: int, string, float, bool i array.
Vrednosti promenljivih ispisati razlicitim bojama.
2.Definisati funkciju koja prima dva parametra, prvi je tipa string i sadr�i engleski naziv boje (red, blue ili green) a drugi parametar je niz sa brojevima.
Funkcija treba da izracuna sumu tih brojeva i da je ispi�e u boji koja je prosledena preko prvog parametra. 
Definiciju funkcije staviti u poseban fajl (functions.php) koji je potrebno ukljuciti u ovaj glavni fajl.