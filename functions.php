<?php
    function zadatak($boja,$niz){
        $suma=0;

        for($x=0;$x<count($niz);$x++){
            $suma+=$niz[$x];
        }
        
        return '<span style="color:'.$boja.'">'.$suma.'</spanp>';
    }

    function minimum($boja,$niz){
        $min=$niz[0];

        for($i=0;$i<count($niz);$i++){
            if($niz[$i]<$min){
                $min=$niz[$i];
            }
        }
        
        return '<span style="color:'.$boja.'">'.$min.'</span>';
    }

    function maximum($boja,$niz){
        $max=$niz[0];

        for($i=0;$i<count($niz);$i++){
            if($niz[$i]>$max){
                $max=$niz[$i];
            }
        }
        return '<span style="color:'.$boja.'">'.$max.'</span>';
    }
?>
